## Version info
VERSION=2.2
DATE=2022-10-11

## Immutable global variables
SCRIPTNAME=$(basename -- "${0}")
DOC_REQUEST=70

###
### Documentation
###
if [ ${#} -eq 0 ] || [ "${1}" = "-h" ] || [ "${1}" = "--help" ]
then
    cat <<USAGEXX
Usage: ${SCRIPTNAME} [options]
   or: ${SCRIPTNAME} addrepos|addusers|delprojects|delrepos|delusers
                    getprojects|getrepos|getusers|set-default-branch
                    [arguments]

Start BitBucketAPI with the specified options, or invoke one of its
tools.

Options:
  -h, --help  Print this help text and exit
  --version   Print version info and exit

Tools:
  addrepos              add new repositories to a project
  addusers              add users to a repository
  delprojects           delete a project
  delrepos              delete repositories from a project
  delusers              delete users from a repository
  getprojects           list the keys of projects
  getrepos              list repositories in a project
  getusers              list users of a repository in a project
  set-default-branch    set the default branch of a repository

Please use '${SCRIPTNAME} tool --help' to obtain further information
about the usage of 'tool'.
USAGEXX
    exit ${DOC_REQUEST}
fi

###
### Version info
###
if [ ${1} = "--version" ]
then
    echo "BitBucketAPI version ${VERSION} (${DATE})
Copyright (C) ${DATE:0:4} Vincent Goulet

BitBucketAPI is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under the terms of the GNU General
Public License versions 2 or 3. For more information about these
matters see https://www.gnu.org/licenses/."
    exit ${DOC_REQUEST}
fi

###
### Script actions
###

## Tool selected at the command line.
tool=${1}

## Create name of helper script.
case "${tool}" in
    addrepos|addusers|delprojects|delrepos|delusers|\
    getprojects|getrepos|getusers|set-default-branch)
	cmd="${SCRIPTNAME}-${tool}"
	;;
    *)
	printf "%s\n" "${SCRIPTNAME}: unsupported  tool '${tool}'"
	exit 1
	;;
esac
shift

## Defer to helper script.
exec "${cmd}" "${@}"

### Local Variables: ***
### mode: sh ***
### End: ***
