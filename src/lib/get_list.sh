###
### get_list <curl_options> <uri> <filename_header> <filename_content>
###
##
##  Retrieve a list from <uri> using curl with options <curl_options>.
##  Store the headers in file <filename_header> and the content in
##  file <filename_content>.
##
##  STANDARD OUTPUT
##
##  Nothing.
##
##  STANDARD ERROR
##
##  Message explaining the source of the error, if any.
##
get_list ()
{
    [ $# -ne 4 ] && return 1
    local curloptions="$1"
    local uri="$2"
    local header="$3"
    local content="$4"

    ## The standard error of curl (the http code) is saved in a
    ## variable, letting standard output through to later redirect it
    ## to a file.
    {
	http_code=$(curl ${curloptions} -D "${header}" \
		       -w "%{stderr}%{http_code}" \
		       "${uri}" 2>&1 1>&3-)
    } 3> "${content}"

    case "$?" in
	0)
	    case "${http_code}" in
		200)
		;;
		401)
		    echo >&2 "authenticated user does not have the required permissions for this project"
		    return 1
		    ;;
		404)
		    echo >&2 "the project does not exist or invalid url"
		    return 1
		    ;;
		500)
		    echo >&2 "incorrect resource url or unexpected server error"
		    return 1
		    ;;
		*)
		    echo >&2 "unknown http code"
		    return 1
		    ;;
	    esac
	    ;;
	6)
	    echo >&2 "could not resolve repository: ${machine}"
	    return 1
	    ;;
	7)
	    echo >&2 "failed to connect to repository: ${machine}"
	    return 1
	    ;;
	*)  
	    echo >&2 "internal curl error"
	    return 1
	    ;;
    esac
}

### Local Variables: ***
### mode: sh ***
### End: ***
