## Immutable global variables
SCRIPTNAME=$(basename -- "${0}")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
DOC_REQUEST=70

if [ $# -eq 0 ]
then
    cat <<USAGEXX
Usage: ${SCRIPTNAME} ${TOOL} [options...] <url>
-h, --help                Show complete help text
[...]                     List of curl options
USAGEXX
    exit ${DOC_REQUEST}
fi

if [ "$1" = "-h" ] || [ "$1" = "--help" ]
then
    cat <<MANPAGEXX
NAME

${SCRIPTNAME} ${TOOL} - list the keys of BitBucket projects

SYNOPSYS

${SCRIPTNAME} ${TOOL} [-h|--help] [<curl-options>] [--] <url>

DESCRIPTION

List the keys of the projects visible for the authenticated user in
the BitBucket server at url (prefix to the REST API) 'url' using the
Atlassian Bitbucket REST API.

The following options are available:

-h, --help
       Show this help text.

<curl options>
       All other options are passed to curl unchanged. Defaults to '-n
       -s'.

REFERENCE

Atlassian Bitbucket REST API Reference
https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html
MANPAGEXX
    exit ${DOC_REQUEST}
fi

## Default values.
curloptions="-s -n"

## Process options.
while [ $# -gt 0 ]
do
    case "$1" in
	--*|-*)
	    case "$2" in
                --|-*) curloptions="${curloptions} $1";;
                *)     curloptions="${curloptions} $1 $2"; shift;;
            esac ;;
	*)
	    break;;		# terminate while loop
    esac
    shift
done

## BitBucket server url is now the script's first argument.
machine="$1"; shift

## Script requires a server url in argument.
! ${machine:+false} || { echo "missing server url" >&2; exit 1; }

## Pagination information appears in the JSON data in the BitBucket
## API. The 'limit' query parameter indicates how many results to
## return per page. The 'start' query parameter indicates which item
## should be used as the first item in the page of results.
limit=100			# standard maximum
start=0				# start with the first page of results

## Create uri to the BitBucket REST API 1.0 to retrieve the list
## of projects in a server instance.
apislug="rest/api/1.0/projects"
uri="https://${machine}/${apislug}?limit=${limit}"

## Create a temporary file to hold the content of the list of
## repositories request.
jsondata=$(mktemp)

while :
do
    ## Retrieve the list of repositories in the project in JSON
    ## format, discarding the headers.
    get_list "${curloptions}" \
	     "${uri}&start=${start}" \
	     "/dev/null" \
	     "${jsondata}" || exit 1
    
    ## Extract only projects names from JSON data.
    grep -E -o '"key":[^,]*' "${jsondata}" | \
	awk 'BEGIN { FS = "\"" } { print $4 }'

    ## Stop if the last page of results is reached; indicated by the
    ## string '"isLastPage":true' in the response.
    grep -E -q '"isLastPage": ?true' "${jsondata}" && break

    ## Find the starting point for the next page of results using the
    ## 'nextPageStart' attribute of the response.
    start=$(grep -E -o '"nextPageStart": ?[0-9]+' "${jsondata}" | \
		cut -d: -f2)
done

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
