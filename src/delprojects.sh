## Immutable global variables
SCRIPTNAME=$(basename -- "${0}")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
DOC_REQUEST=70

if [ $# -eq 0 ]
then
    cat <<USAGEXX
Usage: ${SCRIPTNAME} ${TOOL} [options...] <projectkey>
    --bitbucketrc-file=<filename> Specify FILE for bitbucketrc file
-h, --help                 Show complete help text
-m <url>, --machine=<url>  Set BitBucket server url
-q, --quiet                Quiet mode
[...]                      List of curl options
    --                     Mark the end of options
USAGEXX
    exit ${DOC_REQUEST}
fi

if [ "$1" = "-h" ] || [ "$1" = "--help" ]
then
    cat <<MANPAGEXX
NAME

${SCRIPTNAME} ${TOOL} - delete a BitBucket project

SYNOPSYS

${SCRIPTNAME} ${TOOL} [--bitbucketrc-file=<filename>] [-h|--help]
            [-m <url>|--machine=<url>] [-q|--quiet]
            [<curl-options>] [--] <projectkey>

DESCRIPTION

Delete an existing BitBucket project with key 'projectkey' using the
Atlassian Bitbucket REST API. The project must be empty (no
repositories).

Despite its name, the script only deletes one project for safety
reasons.

By default, the BitBucket server url is read from the '.bitbucketrc'
file in the user's home directory.

The following options are available:

--bitbucketrc-file=<filename>
       Path (absolute or relative) to the bitbucketrc file that the
       script should use (instead of the default '~/.bitbucketrc').

-h, --help
       Show this help text.

-m <url>, --machine=<url>
       BitBucket server url (prefix to the REST API). Overrides the
       default '~./bitbucketrc' file and option --bitbucketrc-file.

-q, --quiet
       Quiet mode: suppress normal output.

<curl options>
       All other options are passed to curl unchanged. Defaults to '-n
       -s'. This script's option '-m' mask the one of curl. Use the
       long forms of these options to pass them to curl.

--
       Marks the end of options, notably curl ones.

FILES

~/.bitbucketrc
       The structure of the file is similar to a '.netrc' file, but
       without authentication information. The 'machine' component
       should contain the host, port and context parts of the server
       URI.

       An example for a project hosted on Université Laval's Faculté
       des sciences et de génie BitBucket server is

       machine projets.fsg.ulaval.ca/git

       This would result in a call to the REST API located at

       https://projets.fsg.ulaval.ca/git/rest/api/1.0/projects/<projectkey>

REFERENCE

Atlassian Bitbucket REST API Reference
https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html
MANPAGEXX
    exit ${DOC_REQUEST}
fi

## Default values.
MACHINE=
CURLOPTIONS="-n -s"
BITBUCKETRC=~/.bitbucketrc

showoutput=true

## Process options.
while [ $# -gt 0 ]
do
    case "$1" in
	-q|--quiet)
	    showoutput=false;;
	--bitbucketrc-file=*)
	    BITBUCKETRC="${1#*=}";;
	-m)
	    MACHINE="$2"; shift;;
	--machine=*)
	    MACHINE="${1#*=}";;
	--)
	    shift; break;;
	--*|-*)
	    case "$2" in
		--|-*) CURLOPTIONS="${CURLOPTIONS} $1";;
		*)     CURLOPTIONS="${CURLOPTIONS} $1 $2"; shift;;
	    esac ;;
	*)
	    break;;		# terminate while loop
    esac
    shift
done

## Project key is now the script's first argument.
PROJECTKEY="$1"; shift

## Script requires a project key in argument.
! ${PROJECTKEY:+false} || { echo "missing project key" >&2; exit 1; }

## Warn that projects other than the first one in argument will not be
## deleted.
[ $# -gt 0 ] && echo "Warning: only the first project will be deleted"

## If the BitBucket server url is not set with option -m, read it from
## the `bitbucketrc` file.
if [ -z "${MACHINE}" ]
then
    MACHINE=$(grep -o 'machine [^ ]*' "${BITBUCKETRC}" | cut -d ' ' -f 2)
fi

## Build REST API url.
URL="https://${MACHINE}/rest/api/1.0/projects/${PROJECTKEY}"

## Create file to hold stderr of curl.
http_code=$(mktemp)

## The numerical response of the retrieved HTTP transfer is
## written to stderr in order to display a useful message in case
## of error.
if curl -X DELETE ${CURLOPTIONS} -w "%{stderr}%{http_code}" "${URL}" >/dev/null 2>"${http_code}"
then
    case $(cat "${http_code}") in
    	204)
    	    ${showoutput} && echo "project ${PROJECTKEY} deleted";;
    	401)
    	    echo "authenticated user has insufficient permissions to delete the project" >&2
    	    exit 1;;
    	404)
    	    echo "the project ${PROJECTKEY} does not exist"
    	    exit 1;;
    	409)
    	    echo "the project ${PROJECTKEY} can not be deleted as it contains repositories"
    	    echo "use 'getrepos.sh' and 'delrepos.sh' to first delete the repositories"
    	    exit 1;;
    esac
else
    echo "internal curl error" >&2
    exit 1
fi

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
