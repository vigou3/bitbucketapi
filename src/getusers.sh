## Immutable global variables
SCRIPTNAME=$(basename -- "${0}")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
DOC_REQUEST=70

if [ $# -eq 0 ]
then
    cat <<USAGEXX
Usage: ${SCRIPTNAME} ${TOOL} [options...] <projectkey> <repository>
    --bitbucketrc-file=<filename> Specify FILE for bitbucketrc file
-h, --help                 Show complete help text
-m <url>, --machine=<url>  Set BitBucket server url
[...]                      List of curl options
    --                     Mark the end of options
USAGEXX
    exit ${DOC_REQUEST}
fi

if [ "$1" = "-h" ] || [ "$1" = "--help" ]
then
    cat <<MANPAGEXX
NAME

${SCRIPTNAME} ${TOOL} - list users of a repository in a BitBucket 
                        project

SYNOPSYS

${SCRIPTNAME} ${TOOL} [--bitbucketrc-file=<filename>] [-h|--help]
            [-m <url>|--machine=<url>] [<curl-options>] [--] 
            <projectkey> <repository>

DESCRIPTION

List the names of users that have been granted at least one permission
for the repository with slug 'repository' in the BitBucket project
with key 'projectkey' using the Atlassian Bitbucket REST API.

By default, the BitBucket server url is read from the '.bitbucketrc'
file in the user's home directory.

The following options are available:

--bitbucketrc-file=<filename>
       Path (absolute or relative) to the bitbucketrc file that the
       script should use (instead of the default '~/.bitbucketrc').

-h, --help
       Show this help text.

-m <url>, --machine=<url>
       BitBucket server url (prefix to the REST API). Overrides the
       default '~./bitbucketrc' file and option --bitbucketrc-file.

<curl options>
       All other options are passed to curl unchanged. Defaults to '-n
       -s'. This script's option '-m' mask the one of curl. Use the
       long forms of these options to pass them to curl.

--
       Marks the end of options, notably curl ones.

FILES

~/.bitbucketrc
       The structure of the file is similar to a '.netrc' file, but
       without authentication information. The 'machine' component
       should contain the host, port and context parts of the server
       URI.

       An example for a project hosted on Université Laval's Faculté
       des sciences et de génie BitBucket server is

       machine projets.fsg.ulaval.ca/git

       This would result in a call to the REST API located at

       https://projets.fsg.ulaval.ca/git/rest/api/1.0/projects/<projectkey>/repos/<repository>

REFERENCE

Atlassian Bitbucket REST API Reference
https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html
MANPAGEXX
    exit ${DOC_REQUEST}
fi

## Default values.
machine=
curloptions="-n -s"
bitbucketrc=~/.bitbucketrc

## Process options.
while [ $# -gt 0 ]
do
    case "$1" in
	--bitbucketrc-file=*)
	    bitbucketrc="${1#*=}";;
	-m)
	    machine="$2"; shift;;
	--machine=*)
	    machine="${1#*=}";;
	--)
	    shift; break;;
	--*|-*)
	       case "$2" in
                   --|-*) curloptions="${curloptions} $1";;
                   *)     curloptions="${curloptions} $1 $2"; shift;;
               esac ;;
	*)
	    break;;		# terminate while loop
    esac
    shift
done

## Project key is now the script's first argument.
projectkey="$1"; shift

## Script requires a project key in argument.
! ${projectkey:+false} || { echo "missing project key" >&2; exit 1; }

## Repository slug is now the script's first argument.
repositoryslug="$1"; shift

## Script requires a repository slug in argument.
! ${repositoryslug:+false} || { echo "missing repository slug" >&2; exit 1; }

## If the BitBucket server url is not set with option -m, read it from
## the `bitbucketrc` file.
if [ -z "${machine}" ]
then
    machine=$(grep -o 'machine [^ ]*' "${bitbucketrc}" | cut -d ' ' -f 2)
fi

## Pagination information appears in the JSON data in the BitBucket
## API. The 'limit' query parameter indicates how many results to
## return per page. The 'start' query parameter indicates which item
## should be used as the first item in the page of results.
limit=100			# standard maximum
start=0				# start with the first page of results

## Create uri to the BitBucket REST API 1.0 to retrieve the list
## of repositories in a project.
apislug="rest/api/1.0/projects"
uri="https://${machine}/${apislug}/${projectkey}/repos/${repositoryslug}/permissions/users?limit=${limit}"

## Create a temporary file to hold the content of the list of
## repositories request.
jsondata=$(mktemp)

while :
do
    ## Retrieve the list of repositories in the project in JSON
    ## format, discarding the headers.
    get_list "${curloptions}" \
	     "${uri}&start=${start}" \
	     "/dev/null" \
	     "${jsondata}" || exit 1
    
    ## Extract only user names from JSON data.
    grep -E -o '"name":[^,]*' "${jsondata}" | \
	awk 'BEGIN { FS = "\"" } { print $4 }'

    ## Stop if the last page of results is reached; indicated by the
    ## string '"isLastPage":true' in the response.
    grep -E -q '"isLastPage": ?true' "${jsondata}" && break

    ## Find the starting point for the next page of results using the
    ## 'nextPageStart' attribute of the response.
    start=$(grep -E -o '"nextPageStart": ?[0-9]+' "${jsondata}" | \
		cut -d: -f2)
done

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
