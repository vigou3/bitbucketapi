### -*-Makefile-*- to prepare and release BitBucketAPI
##
## Copyright (C) 2021 Vincent Goulet
##
## 'make scripts' builds the scripts.
##
## 'make install' installs the scripts locally.
##
## 'make zip' builds the a .zip archive of the scripts.
##
## 'make release' creates a release in GitLab and uploads the archive.
##
## 'make all' is equivalent to 'make zip' to avoid accidendal
## releases.
##
## Author: Vincent Goulet
##
## This file is part of the project BitBucketAPI
## https://gitlab.com/vigou3/bitbucketapi

## Files
MAIN := bitbucketapi
TOOLS := $(addprefix ${MAIN}-,addrepos addusers delprojects delrepos delusers getprojects getrepos getusers set-default-branch)
README := README.md
NEWS := NEWS
LICENSE := LICENSE
HEADER := share/header.txt

## Installation directories
INSTDIR := inst
PREFIX := /usr/local
BINDIR := ${PREFIX}/bin
DOCDIR := ${PREFIX}/share/doc/${MAIN}

## Version info read from the main script source file
VERSION := $(shell awk 'BEGIN { FS="=" } /^VERSION=/ { print $$2 }' src/${MAIN}.sh)
DATE := $(shell awk 'BEGIN { FS="=" } /^DATE=/ { print $$2 }' src/${MAIN}.sh)
YEAR := $(word 1,$(subst -, ,${DATE}))

## Package name
ZIP := ${MAIN}-${VERSION}.zip

## Toolset
CP := cp -p
RM := rm -rf
MD := mkdir -p

## Temporary directory to build the archive
BUILDDIR := builddir

## Dépôt GitLab et authentification
REPOSURL := https://gitlab.com/vigou3/bitbucketapi
REPOSNAME := $(shell basename ${REPOSURL})
APIURL := https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN := $(shell cat ~/.gitlab/token)

## Variables automatiques
TAGNAME := v$(firstword ${VERSION})


all: zip

${INSTDIR}${BINDIR}/${MAIN}: src/${MAIN}.sh ${HEADER}
	[ -d ${INSTDIR}${BINDIR} ] || ${MD} ${INSTDIR}${BINDIR}
	printf > "$@" "%s\n\n" "#!/bin/sh" && \
	  cat >> "$@" ${HEADER} && \
	  echo "" | cat - $(filter src/%,$^) | \
	    sed '/###.*\*\*\*$$/d' | \
	    sed -e :a -e '/^\n*$$/N;/\n$$/ba' \
	    >> "$@"
	chmod 755 "$@"

$(addprefix ${INSTDIR}${BINDIR}/,${TOOLS}): ${HEADER}
	[ -d ${INSTDIR}${BINDIR} ] || ${MD} ${INSTDIR}${BINDIR}
	printf > "$@" "%s\n\n" "#!/bin/sh" && \
	  cat >> "$@" ${HEADER} && \
	  for f in $(filter src/lib/%,$^); \
	      do echo; cat $$f; \
	  done >> "$@" && \
	  echo "" | cat - $(filter %.sh,$(filter-out share/% src/lib/%,$^)) | \
	    sed '/###.*\*\*\*$$/d' | \
	    sed -e :a -e '/^\n*$$/N;/\n$$/ba' \
	    >> "$@"
	chmod 755 "$@"

${INSTDIR}${BINDIR}/${MAIN}-addrepos: src/addrepos.sh
${INSTDIR}${BINDIR}/${MAIN}-addusers: src/addusers.sh
${INSTDIR}${BINDIR}/${MAIN}-delprojects: src/delprojects.sh
${INSTDIR}${BINDIR}/${MAIN}-delrepos: src/delrepos.sh
${INSTDIR}${BINDIR}/${MAIN}-delusers: src/delusers.sh
${INSTDIR}${BINDIR}/${MAIN}-getprojects: src/getprojects.sh src/lib/get_list.sh
${INSTDIR}${BINDIR}/${MAIN}-getrepos: src/getrepos.sh src/lib/get_list.sh
${INSTDIR}${BINDIR}/${MAIN}-getusers: src/getusers.sh src/lib/get_list.sh
${INSTDIR}${BINDIR}/${MAIN}-set-default-branch: src/set-default-branch.sh

.PHONY: scripts
scripts: ${INSTDIR}${BINDIR}/${MAIN} \
         $(addprefix ${INSTDIR}${BINDIR}/,${TOOLS})

.PHONY: release
release: update-copyright zip check-status create-release upload create-link

.PHONY: update-copyright
update-copyright: ${HEADER}
	for f in $?; \
	    do sed -E '/^#* +Copyright \(C\)/s/20[0-9]{2}/${YEAR}/' \
	           $$f > $$f.tmp && \
	           ${CP} $$f.tmp $$f && \
	           ${RM} $$f.tmp; \
	done

.PHONY: install
install: scripts
	tar -C ${INSTDIR} -cf - . | tar -C / -xmf - 

.PHONY: zip
zip: scripts ${README} ${NEWS} ${LICENSE}
	if [ -d ${BUILDDIR} ]; then ${RM} ${BUILDDIR}; fi
	${MD} $(addprefix ${BUILDDIR},${BINDIR} ${DOCDIR})
	tar -C ${INSTDIR} -cf - . | tar -C ${BUILDDIR} -xmf - 
	pandoc -o ${BUILDDIR}${DOCDIR}/${README:.md=.html} ${README}
	${CP} ${NEWS} ${LICENSE} ${BUILDDIR}${DOCDIR}
	cd ${BUILDDIR} && zip --filesync -r ../${ZIP} *
	${RM} ${BUILDDIR}

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "----- Checking status of working directory... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "master"  ] && [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "not on branch master or main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "uncommitted changes in repository; not creating release"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/master..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "unpushed commits in repository; pushing to origin"; \
	        git push; \
	    else \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: create-release
create-release:
	@{ \
	    printf "%s" "checking if a release already exists... "; \
	    http_code=$$(curl -I "${APIURL}/releases/${TAGNAME}" 2>/dev/null \
	                     | head -n1 | cut -d " " -f2) ; \
	    if [ "$${http_code}" = "200" ]; \
	    then \
	        printf "%s\n" "yes"; \
	        printf "%s\n" "-> using the existing release"; \
	    else \
	        printf "%s\n" "no"; \
	        printf "%s" "creating a release in GitLab... "; \
	        name=$$(awk '/^# / { sub(/# +/, "", $$0); print $$0; exit }' ${NEWS}); \
	        desc=$$(awk ' \
	                      /^$$/ { next } \
	                      (state == 0) && /^# / { state = 1; next } \
	                      (state == 1) && /^# / { exit } \
	                      (state == 1) { print } \
	                    ' ${NEWS}); \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master" && \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --data tag_name="${TAGNAME}" \
	             --data name="$${name}" \
	             --data description="$${desc}" \
	             --output /dev/null --silent \
	             "${APIURL}/releases"; \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: upload
upload:
	@printf "%s\n" "uploading asset to the package registry..."
	curl --upload-file "${ZIP}" \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --silent \
	     "${APIURL}/packages/generic/${REPOSNAME}/${VERSION}/${ZIP}"
	@printf "\n%s\n" "ok"


.PHONY: create-link
create-link: create-release
	@printf "%s" "adding link to the asset to the release... "
	$(eval pkg_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           "${APIURL}/packages" \
	                      | grep -o -E '\{.*"version":"${VERSION}"[^}]*}' \
	                      | grep -o '"id":[0-9]*' | cut -d: -f 2))
	$(eval file_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                            --silent \
	                            "${APIURL}/packages/${pkg_id}/package_files" \
	                       | grep -o -E '\{.*"file_name":"${ZIP}"[^}]*}' \
	                       | grep -o '"id":[0-9]*' | cut -d: -f 2))
	@curl --request POST \
	      --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	      --data name="${ZIP}" \
	      --data url="${REPOSURL}/-/package_files/${file_id}/download" \
	      --data link_type="package" \
	      --output /dev/null --silent \
	      "${APIURL}/releases/${TAGNAME}/assets/links"
	@printf "%s\n" "ok"
