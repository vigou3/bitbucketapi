# BitBucketAPI &mdash; Interface to the Atlassian BitBucket REST API

BitBucketAPI provides Unix shell scripts to achieve select
administrative operations on a BitBucket repository using the
[Atlassian REST API](https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html).

- `bitbucketapi` is the unified interface to the tools below;

- `bitbucketapi addusers` promotes or demotes one or many users'
  permission level for a repository.

- `bitbucketapi delusers` revokes all permissions of one or many users
  for a repository.

- `bitbucketapi getusers` lists the names of users that have been
  granted at least one permission for a repository.

- `bitbucketapi addrepos` creates new repositories in an existing
  BitBucket project.

- `bitbucketapi delrepos` deletes repositories in an existing
  BitBucket project.

- `bitbucketapi getrepos` lists repositories in an existing BitBucket
  project.

- `bitbucketapi getprojects` lists visible projects in the BitBucket
  server.

- `bitbucketapi delprojects` deletes an empty BitBucket project (only
  one at a time despite the name).

The scripts require `curl`. They are developed, tested and used on
macOS, but they should run on any POSIX shell.

> BitBucketAPI derives from and replaces the now obsolete project [bitbucket-scripts](https://gitlab.com/vigou3/bitbucket-scripts).

## Author

Vincent Goulet, École d'actuariat, Université Laval

## Installing

Get the `.zip` archive of the
[latest stable release](https://gitlab.com/vigou3/bitbucketapi/-/releases)
from GitLab and unzip the archive into the root of the filesystem. On
Unix-alike systems (including macOS):
```
unzip -d / bitbucketapi-x.y.zip
```

On Windows, BitBucketAPI requires a Unix shell such as MSYS2 or Git
Bash. From the shell point of view, the root of the filesystem is
the installation directory of the shell. For example, if MSYS2 is
installed in `C:\msys2`:
```
unzip -d c:/msys2 bitbucketapi-x.y.zip
```

(Replace `x.y` by the actual version number.)

## Disclaimer

The scripts were written for my own use and they serve me well. They
are provided here in case they may be useful to anyone.

Use at your own risk.
